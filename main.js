//chart options
const options = {
    chart: {
        height : 450,
        width : '100%',
        type : 'bar',
        background: '#f4f4f4',
        foreColor: '#333',
        toolbar: {
            show: true,
            tools: {
              download: true,
              selection: true,
              zoom: true,
              zoomin: true,
              zoomout: true,
              pan: true,
              reset: true
            },
            autoSelected: 'zoom'
          },

    },
    
    series: [{
        name: 'Per 1000 live births',
        data : [58.54, 55.98, 53.38, 50.80, 48.29, 45.93, 43.78, 41.87, 40.21, 38.77, 37.54, 36.51 ]
    }],
    xaxis: {
        categories: [2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015]
    },
    plotOptions:{
        bar: {
            horizontal: false
        }  
        

    },
    fill: {
        type: 'pattern',
                opacity: 1,
                pattern: {
                    style:['slantedLines'],
                }
        // colors: ['#f44336']
        // theme: {
        //     palette: 'palette1'
        // },

    },
    dataLabels: {
        enabled: false
    },
    title:{
        text: 'Infant Mortality Rate in Kenya',
        align: 'center',
        margin: 20,
        offsetY: 20,
        
        style:{
            fontSize: 35,
            color: '#085da8'
        }

    },
    theme: {
        palette: 'palette1', 
        monochrome: {
            enabled: false,
            color: '#2478ee',
            shadeTo: 'dark',
            shadeIntensity: 0.65
        },
    }
    
    
};

//init chart
const chart = new ApexCharts(document.querySelector('#chart'), options);

//render chart
chart.render();